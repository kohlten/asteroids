from sys import argv
from os import listdir, path, makedirs

def main():
    if len(argv) == 1:
        print("python create_dirs.py <base_dir> <obj_dir> <ignored_dirs> ...")
        return
    base_dir = argv[1]
    obj_dir = argv[2]
    ignored_dirs = argv[2:]
    all = listdir(base_dir)
    for file in all:
        if file not in ignored_dirs and path.isdir(file):
            makedirs(obj_dir + "/" + file)

if __name__ == '__main__':
    main()