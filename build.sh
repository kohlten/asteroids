#!/bin/bash
set -e
if [ $# != 1 ]; then
        echo "sh build.sh BINARY"
        exit 1
fi
if [ ! -d "subprojects" ]; then
	mkdir subprojects
fi
if [ ! -d "subprojects/libft" ]; then
	git clone https://gitlab.com/kohlten/libft.git subprojects/libft
fi
if [ ! -d "subprojects/libgame" ]; then
	git clone https://gitlab.com/kohlten/libgame.git subprojects/libgame
fi
system=$(uname -a | awk '{print $1}')
if [ "$system" == "Darwin" ]; then
    cores=$(sysctl -n hw.ncpu)
elif [ "$system" == "Linux" ]; then
    cores=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')
fi
if type meson &>/dev/null; then
    cd subprojects/libft && meson build && cd build && ninja
    cd ../../../
    cd subprojects/libgame && meson build && cd build && ninja
    cd ../../../
    meson build
    cd build
    ninja
    ninja test
    ninja install
    cd ..
elif type make &>/dev/null; then
    cd subprojects/libft && make -j $cores
    cd ../../
    echo $(pwd)
    cd subprojects/libgame && make clean && make -j $cores
    cd ../../
    make re
else
    echo "You don't have meson or make on your system. Unable to build."
    exit 1
fi
cp "build/$1" "."
