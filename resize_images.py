from sys import argv
from os import path, listdir
from PIL import Image

filetypes = ["jpg", "png"]

def resize_images(names, size):
	for file in names:
		if file.split(".")[-1] not in filetypes:
			print(file)
			print("Unknown image type!")
			return

	try:
		size = [int(num) for num in size.split(":")]
	except:
		print("Bad size")
		return

	for file in names:
		img = Image.open(file)
		img = img.resize((size[0], size[1]))
		img.save(file)
		img.close()

def main():
	if len(argv) != 3:
		print("python resize_images.py FOLDER_NAME W:H")
		return 0
	dirpath = argv[1]
	size = argv[2]
	if not path.isdir(dirpath):
		print("Can not do single images!")
		return 0
	print(len(size.split(":")))
	if len(size.split(":")) != 2:
		print("Unkown size")
		return 0
	resize_images([dirpath + "/" + name for name in listdir(dirpath)], size)

if __name__ == '__main__':
	main()