from sys import argv, exit
from PIL import Image

def create_images(src_image_name, output_dir, angle, frames):
    src_image = Image.open(src_image_name)
    max_size = [0, 0]
    if src_image.size[0] > src_image.size[1]:
        max_size[0] = src_image.size[0]
        max_size[1] = src_image.size[0]
    else:
        max_size[0] = src_image.size[1]
        max_size[1] = src_image.size[1]
    resized_image = Image.new("RGBA", tuple(max_size), color=(0, 0, 0, 0))
    resized_image.paste(src_image, [int((max_size[0] / 2) - (src_image.size[0] / 2)), int((max_size[1] / 2) - (src_image.size[1] / 2))])
    src_image = resized_image
    current_angle = 0
    print("ANGLE INCREASE: ", angle / frames)
    for i in range(0, frames):
        new_image = src_image.rotate(current_angle, expand=False)
        new_image.save(output_dir + "/" + str(i) + ".png")
        current_angle += angle / frames

def main():
    if len(argv) < 5:
        print("python3 rotate_images.py src_img output_dir to_angle frames")
        return -1
    src_image = argv[1]
    output_dir = argv[2]
    try:
        angle = int(argv[3])
        if angle < 0 or angle > 360:
            raise Exception
    except:
        print("Invalid Angle")
        return -1
    try:
        frames = int(argv[4])
    except:
        print("Invalid frame number")
        return -1
    return create_images(src_image, output_dir, angle, frames)

if __name__ == '__main__':
    exit(main())