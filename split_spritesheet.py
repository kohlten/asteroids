from sys import argv
from os import path, listdir
from PIL import Image

filetypes = ["jpg", "png"]

def get_images(name, size, output_dir):
	if name.split(".")[-1] not in filetypes:
		print(file)
		print("Unknown image type!")
		return
	num = 0
	with Image.open(name) as im:
		width, height = im.size
	max_width =  width / size[0]
	max_height = height /size[1]
	for i in range(int(max_width)):
		for j in range(int(max_height)):
			image = Image.open(name)
			x = i * size[0]
			y = j * size[1]
			w = size[0] + x
			h = size[1] + y
			image = image.crop((x, y, w, h))
			out_name = f"{output_dir}/{num}.png"
			image.save(out_name)
			num += 1

def main():
	if len(argv) != 4:
		print("python resize_images.py SPRITESHEET WIDTH:HEIGHT OUTPUT_DIR")
		return 0
	file = argv[1]
	try:
		size = [int(argv[2].split(':')[0]), int(argv[2].split(':')[1])]
	except:
		print("Unkown size")
		return 0
	if path.isdir(file):
		print("Can not do directories")
		return 0
	if not path.isdir(argv[3]):
		print("Invalid output dir")
		return 0
	get_images(file, size, argv[3])

if __name__ == '__main__':
	main()