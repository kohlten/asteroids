//
// Created by kohlten on 1/14/19.
//

#include "menu_scene.h"
#include "assert.h"

void init_menu_scene(t_menu_scene *scene, t_window *window, int fps) {
    scene->button_reg = new_surface("images/PNG/UI/button.png");
    assert(scene->button_reg != NULL, "Failed to load button image!");
    scene->button_hover = new_surface("images/PNG/UI/buttonGreen_hover.png");
    assert(scene->button_hover != NULL, "Failed to load button image!");
    assert(init_text_collage_json(&scene->text, "fonts/fasttracker2-style_12x12.json", scene->button_reg->format) == 0,
                             "Failed to init text collage!\n");
    assert(init_gui_button(&scene->play_button, &scene->text,
                           new_vector2i(window->width / 2 - scene->button_hover->size.x / 2,
                                        window->height / 2 - scene->button_hover->size.y / 2), scene->button_reg->size,
                           scene->button_reg, scene->button_hover, NULL) == 0, "Failed to init button!\n");
    assert(change_string_gui_button(&scene->play_button, "Play") == 0, "Failed to change the string!\n");
    init_fps(&scene->update_fps, 60);
    init_fps(&scene->render_fps, fps);
    scene->background = new_texture(window->SDLrenderer, "images/resized_background.png");
    assert(scene->background != NULL, "Failed to load background!\n");
    scene->background_pos = 0;
    scene->exited = false;
}

void menu_scene_events(t_menu_scene *scene, t_vector2i mouse_pos, bool mouse_pressed) {
    update_gui_button(&scene->play_button, mouse_pos, mouse_pressed);
    if (scene->play_button.clicked)
        scene->exited = true;
}

void menu_scene_update(t_menu_scene *scene) {
    limit_fps(&scene->update_fps);
    update_fps(&scene->update_fps);
    if (scene->background_pos <= -scene->background->width)
        scene->background_pos = 0;
    scene->background_pos -= 0.5;
}

void menu_scene_draw(t_menu_scene *scene, SDL_Renderer *renderer, t_vector2i window_size, bool aa, bool debug) {
    t_vector2i text_size;

    limit_fps(&scene->render_fps);
    update_fps(&scene->render_fps);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    render_texture(scene->background, renderer, new_vector2i((int) scene->background_pos + scene->background->width, 0),
                   NULL, 0, new_vector2i(0, 0), SDL_FLIP_NONE);
    render_texture(scene->background, renderer, new_vector2i((int) scene->background_pos, 0), NULL, 0,
                   new_vector2i(0, 0), SDL_FLIP_NONE);
    text_size = get_text_size_text_collage(&scene->text, "Asteroids", 2.0);
    draw_text_collage(&scene->text, "Asteroids", renderer, new_vector2i((window_size.x / 2) - text_size.x / 2, window_size.y / 3),
                      0, 2.0, aa, NULL, 0);
    draw_gui_button(&scene->play_button, renderer, 0, 1.0, aa, debug);
    SDL_RenderPresent(renderer);
}

void menu_scene_reset(t_menu_scene *scene) {
    reset_gui_button(&scene->play_button);
}

void menu_scene_free(t_menu_scene *scene) {
    free_text_collage(&scene->text);
    free_gui_button(&scene->play_button);
    free_surface(scene->button_hover);
    free_surface(scene->button_reg);
}
