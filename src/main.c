#include "game.h"

/*
 * Add config for different values instead of being static values
 * Add a Menu
    * Add a GUI in the menu to change settings
    * Add highscore GUI in menu
 * Add a menu for end game and have highscores on it
 * Add config for key binds
 ** Add a highscore system
 ****** Clean up code
 */

// For windows, SDL2 creates a main
//#undef main

#define SODIUM_STATIC

// Have to invoke main this way for windows
int main(int argc, char *argv[])
{
    t_game game;

    (void) argc;
    (void) argv;
    ft_bzero(&game, sizeof(t_game));
    init_game(&game);
    run(&game);
    free_game(&game);
    return 0;
}