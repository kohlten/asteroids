//
// Created by kohlten on 1/14/19.
//

#ifndef asteroids_MENU_SCENE_H
#define asteroids_MENU_SCENE_H

#include "rendering/gui/gui_button.h"
#include "rendering/text/text_collage.h"
#include "fps.h"
#include "window.h"
#include "surface.h"
#include "texture.h"

typedef struct s_menu_scene {
	t_texture *background;
    t_surface *button_reg;
    t_surface *button_hover;
    t_text_collage text;
    t_gui_button play_button;
    t_fps update_fps;
    t_fps render_fps;
    double background_pos;
    bool exited;
}               t_menu_scene;

void init_menu_scene(t_menu_scene *scene, t_window *window, int fps);
void menu_scene_events(t_menu_scene *scene, t_vector2i mouse_pos, bool mouse_pressed);
void menu_scene_update(t_menu_scene *scene);
void menu_scene_draw(t_menu_scene *scene, SDL_Renderer *renderer, t_vector2i window_size, bool aa, bool debug);
void menu_scene_reset(t_menu_scene *scene);
void menu_scene_free(t_menu_scene *scene);

#endif //asteroids_MENU_SCENE_H
