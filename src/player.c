//
// Created by Alexander Strole on 12/1/18.
//
#include <SDL.h>
#include <SDL2_gfxPrimitives.h>
#include <stdbool.h>
#include "player.h"
#include "libft.h"
#include "bullet.h"
#include "asteroid.h"
#include "assert.h"
#include "ufo.h"

static void load_player_images(t_player *player) {
    vector thruster_images;
    vector player_images;
    vector shield_images;
    t_surface *tmp;
    char *current;
    char *joined;
    char *full_joined;
    int i;

    vector_init(&thruster_images);
    for (i = 0; i <= 2; i++) {
        current = ft_itoa(i);
        joined = ft_strjoin("images/PNG/player_ship1_green_running", current);
        full_joined = ft_strjoin(joined, ".png");
        tmp = new_surface(full_joined);
        assert(tmp != NULL, "");
        vector_add(&thruster_images, tmp);
        free(current);
        free(joined);
        free(full_joined);
    }
    for (i = 2; i >= 0; i--) {
        tmp = new_from_surface(((t_surface *) vector_get(&thruster_images, i))->surface);
        assert(tmp != NULL, "Failed to copy image!\n");
        vector_add(&thruster_images, tmp);
    }
    init_animator(&player->animator);
    assert(add_state_animator(&player->animator, "thrusting", &thruster_images, 50, 0, -1) == 0,
           "Failed to add animator state!\n");
    vector_free(&thruster_images);
    vector_init(&player_images);
    tmp = new_surface("images/PNG/playerShip1_green.png");
    assert(tmp != NULL, "Failed to get player image!\n");
    vector_add(&player_images, tmp);
    assert(add_state_animator(&player->animator, "default", &player_images, 50, 0, -1) == 0,
           "Failed to add animator state!\n");
    change_state_animator(&player->animator, "default");
    player->bullet_texture = new_surface("images/PNG/Lasers/laserBlue01.png");
    assert(player->bullet_texture != NULL, "Failed to load laser image!\n");
    init_animator(&player->shield_animator);
    vector_init(&shield_images);
    for (i = 0; i < 100; i++) {
        current = ft_itoa(i);
        joined = ft_strjoin("images/player_shield/", current);
        full_joined = ft_strjoin(joined, ".png");
        tmp = new_surface(full_joined);
        assert(tmp != NULL, "Failed to load player shield!");
        vector_add(&shield_images, tmp);
        free(current);
        free(joined);
        free(full_joined);
    }
    assert(add_state_animator(&player->shield_animator, "default", &shield_images, 50, 0, -1) == 0,
           "Failed to add animator state!\n");
    change_state_animator(&player->shield_animator, "default");
    start_playing_animator(&player->shield_animator);
    vector_free(&shield_images);
}

// @TODO Move some functions to another file; Maybe when we add different bullets but all bullet mechanics in there?
t_player *new_player(t_vector2i size) {
    t_player *player;
    t_vector2i frame_size;

    player = ft_memalloc(sizeof(t_player));
    if (!player)
        return (NULL);
    init_physics_object(&player->object);
    load_player_images(player);
    vector_init(&player->bullets);
    init_timer(&player->death_timer, 1000);
    init_timer(&player->bullet_timer, 150);
    init_timer(&player->shield_timer, 500);
    reset_player(player, size);
    frame_size = get_frame_size(&player->animator);
    player->dist = ((double) frame_size.x * 0.5) * 0.5;
    player->angle = 0;
    player->lives = 0;
    player->shield_energy = 100;
    player->god_mode = false;
    player->score = 0;
    player->shielded = false;
    return player;
}

static void check_for_bullet_collisons(vector *bullets, vector *asteroids, int *player_score) {
    t_asteroid *asteroid;
    t_bullet *bullet;
    int i;
    int j;

    i = 0;
    while (i < vector_total(bullets)) {
        bullet = vector_get(bullets, i);
        j = 0;
        while (j < vector_total(asteroids)) {
            asteroid = vector_get(asteroids, j);
            if (dist_vector(asteroid->object.pos, bullet->object.pos) <= asteroid->total_radius) {
                bullet->died = 1;
                asteroid->health--;
                if (asteroid->health <= 0) {
                    if (asteroid->size == 1)
                        *player_score += 100;
                    else if (asteroid->size == 2)
                        *player_score += 50;
                    else if (asteroid->size == 3)
                        *player_score += 20;
                }
            }
            j++;
        }
        i++;
    }
}

static void update_player_bullets(vector *bullets, t_vector2i size) {
    int i;
    t_bullet *bullet;

    i = 0;
    while (i < vector_total(bullets)) {
        bullet = vector_get(bullets, i);
        update_bullet(bullet, size);
        if (bullet->died) {
            free_bullet(bullet);
            vector_delete(bullets, i);
        } else
            i++;
    }
}

void shoot(t_player *player) {
    t_bullet *bullet;
    t_vector2f new_pos;

    if (timer_done(&player->bullet_timer) && player->spawned == true) {
        new_pos = rotate_vector2f(new_vector2f(player->object.pos.x + player->dist, player->object.pos.y),
                                  player->object.pos, player->angle);
        bullet = new_bullet(player->bullet_texture, new_pos, player->angle);
        assert(bullet != NULL, "Failed to malloc a new bullet!\n");
        vector_add(&player->bullets, bullet);
        player->added_bullets++;
        start_timer(&player->bullet_timer);
    }
}

static void check_for_death(t_player *player, vector *asteroids, vector *ufos, t_vector2i size) {
    int i, j;
    t_asteroid *asteroid;
    t_ufo *ufo;
    t_bullet *bullet;
    bool on = false;
    double max;
    t_vector2f middle;

    for (i = 0; i < vector_total(asteroids); i++) {
        asteroid = vector_get(asteroids, i);
        if (!player->god_mode) {
            if (player->spawned) {
                if (dist_vector(asteroid->object.pos, player->object.pos) <= player->dist + asteroid->radius) {
                    if (!player->shielded) {
                        reset_player(player, size);
                        player->lives--;
                    } else
                        asteroid->health = 0;
                }
            } else {
                middle = new_vector2f((float) size.x / 2, (float) size.y / 2);
                max = 110 * mag_vector(asteroid->object.vel);
                if (dist_vector(middle, asteroid->object.pos) <= max)
                    on = true;
            }
        }
    }
    if (!player->god_mode) {
        for (i = 0; i < vector_total(ufos); i++) {
            ufo = vector_get(ufos, i);
            for (j = 0; j < vector_total(&ufo->bullets); j++) {
                bullet = vector_get(&ufo->bullets, j);
                if (dist_vector(player->object.pos, bullet->object.pos) <= player->dist) {
                    bullet->died = 1;
                    if (!player->shielded) {
                        reset_player(player, size);
                        player->lives--;
                    }
                }
            }
        }
    }
    if (player->spawned == false && on == true)
        reset_timer(&player->death_timer);
    if (player->spawned == false && on == false && timer_done(&player->death_timer))
        player->spawned = true;
}

// @TODO Separate this function out
// @TODO Player ship collisions appear to be working only part of the time
void update_player(t_player *player, vector *asteroids, vector *ufos, t_vector2i size) {
    check_for_bullet_collisons(&player->bullets, asteroids, &player->score);
    check_for_death(player, asteroids, ufos, size);
    update_player_bullets(&player->bullets, size);
    if (player->spawned) {
        if (player->shooting)
            shoot(player);
        if (player->moving)
            apply_force(&player->object, vector2f_from_angle(player->angle, 0.15));
        if (player->rotating) {
            if (player->rotation_direction == 1)
                player->angle -= 0.05;
            else if (player->rotation_direction == 2)
                player->angle += 0.05;
        }
        if (player->angle < 0)
            player->angle = (M_PI * 2);
        if (player->angle > M_PI * 2)
            player->angle = 0;
        if (player->object.pos.x < 0)
            player->object.pos.x = size.x;
        if (player->object.pos.x > size.x)
            player->object.pos.x = 0;
        if (player->object.pos.y < 0)
            player->object.pos.y = size.y;
        if (player->object.pos.y > size.y)
            player->object.pos.y = 0;
        player->object.vel = mul_by_vector2f(player->object.vel, 0.97);
        player->object.vel = limit_vector2f(player->object.vel, 3.0);
        update_object(&player->object);
        if (player->shielded)
            player->shield_energy -= 0.35;
        else if (player->shield_energy < 100)
            player->shield_energy += 0.04;
        if (timer_done(&player->shield_timer))
            player->shielded = false;
    }
    update_timer(&player->death_timer);
    update_timer(&player->bullet_timer);
    update_timer(&player->shield_timer);
    update_animator(&player->animator);
    update_animator(&player->shield_animator);
}

void change_dir(t_player *player, char dir, int keydown) {
    switch (dir) {
        case 'w':
            if (keydown == 1) {
                player->moving = true;
                if (!is_playing_animator(&player->animator) &&
                    ft_strcmp(get_current_state_animator(&player->animator), "thrusting") != 0) {
                    change_state_animator(&player->animator, "thrusting");
                    start_playing_animator(&player->animator);
                }
            } else {
                player->moving = false;
                if (is_playing_animator(&player->animator) &&
                    ft_strcmp(get_current_state_animator(&player->animator), "thrusting") == 0) {
                    change_state_animator(&player->animator, "default");
                    stop_playing_animator(&player->animator);
                }
            }
            break;
        case 'a':
            if (keydown == 1) {
                player->rotation_direction = 1;
                player->rotating = true;
            } else
                player->rotating = false;
            break;
        case 'd':
            if (keydown == 1) {
                player->rotation_direction = 2;
                player->rotating = true;
            } else
                player->rotating = false;
            break;
        case ' ':
            if (keydown == 1)
                player->shooting = true;
            else
                player->shooting = false;
            break;
        case 's':
            if (player->shield_energy > 0) {
                if (keydown == 1) {
                        player->shielded = true;
                        start_timer(&player->shield_timer);
                }
            }
        default:
            break;
    }
}

static void draw_player_bullets(vector *bullets, SDL_Renderer *renderer, bool aa, bool debug) {
    int i;
    t_bullet *bullet;

    i = 0;
    while (i < vector_total(bullets)) {
        bullet = vector_get(bullets, i);
        draw_bullet(bullet, renderer, aa, debug);
        i++;
    }
}

void draw_player(t_player *player, SDL_Renderer *renderer, bool aa, bool debug) {
    t_surface *rotated;
    t_surface *current;
    t_vector2f bullet_pos;
    double angle;

    if (player->spawned) {
        angle = map(player->angle + (M_PI / 2), new_vector2f(0, M_PI * 2), new_vector2f(360, 0));
        draw_player_bullets(&player->bullets, renderer, aa, debug);
        current = get_current_image_animator(&player->animator);
        if (!current) return;
        rotated = rotate_surface(current, angle, 0.5, aa);
        assert(rotated != NULL, "Failed to rotate image!\n");
        draw_surface(rotated, renderer, new_vector2i((int) (player->object.pos.x - (rotated->size.x / 2)),
                                                     (int) (player->object.pos.y - (rotated->size.y / 2))));
        free_surface(rotated);
        if (player->shielded) {
            current = get_current_image_animator(&player->shield_animator);
            if (!current) return;
            rotated = rotate_surface(current, angle, 1.0, aa);
            draw_surface(rotated, renderer, new_vector2i((int) player->object.pos.x - (rotated->size.x / 2),
                                                         (int) player->object.pos.y - (rotated->size.y / 2)));
            free_surface(rotated);
        }
        if (debug) {
            bullet_pos = rotate_vector2f(new_vector2f(player->object.pos.x + player->dist, player->object.pos.y),
                                         player->object.pos, player->angle);
            if (aa) {
                aacircleRGBA(renderer, (int) player->object.pos.x,
                             (int) player->object.pos.y,
                             (int) player->dist, 0, 0, 255, 255);
                aacircleRGBA(renderer, (int) bullet_pos.x, (int) bullet_pos.y, (int) 5, 255, 0, 0, 255);
            } else {
                circleRGBA(renderer, (int) player->object.pos.x,
                           (int) player->object.pos.y,
                           (int) player->dist, 0, 0, 255, 255);
                circleRGBA(renderer, (int) bullet_pos.x, (int) bullet_pos.y, (int) 5, 255, 0, 0, 255);
            }
        }
    }

}

void reset_player(t_player *player, t_vector2i size) {
    start_timer(&player->death_timer);
    player->shielded = false;
    player->object.vel = vector2f_zero;
    player->object.acc = vector2f_zero;
    player->object.pos = new_vector2f((float) size.x / 2, (float) size.y / 2);
    player->spawned = false;
}

void free_player(t_player *player) {
    for (int i = 0; i < vector_total(&player->bullets); i++)
        free_bullet(vector_get(&player->bullets, i));
    free_animator(&player->animator, true);
    vector_free(&player->bullets);
    free(player);
}