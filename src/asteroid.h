//
// Created by Alexander Strole on 12/1/18.
//

#ifndef asteroids_asteroid_H
#define asteroids_asteroid_H
#include <SDL.h>
#include "vector/vector.h"
#include "vector/physics_object.h"
#include "timer.h"
#include "data_structures/vector_array.h"
#include "rendering/animator.h"

typedef struct s_asteroid
{
    t_physics_object object;
    t_animator  animator;
    int         verts_len;
    double      radius;
    double      total_radius;
    int         size;
    int         health;
    t_timer     teleport_wait;
}               t_asteroid;

t_asteroid *new_asteroid(t_vector2f pos, t_vector2i window_size, double radius, int size, t_vector2f player_pos, vector *asteroid_images);
void draw_asteroid(t_asteroid *asteroid, SDL_Renderer *renderer, bool aa, bool debug);
void update_asteroid(t_asteroid *asteroid, t_vector2i size);
void free_asteroid(void *data);

void spawn_new_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images);
int break_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images);
int update_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images);
void draw_asteroids(vector *asteroids, SDL_Renderer *renderer, bool aa, bool debug);
int count_large_asteroids(vector *asteroids);
void free_asteroids(vector *asteroids);

#endif //asteroids_asteroid_H
