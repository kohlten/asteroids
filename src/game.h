#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "libft.h"
#include "fps.h"
#include "window.h"
#include "rendering/text/text.h"
#include "asteroid.h"
#include "player.h"
#include "data_structures/vector_array.h"
#include "config.h"

#include "game_scene.h"
#include "menu_scene.h"

typedef enum {
	GAME_SCENE,
	MENU_SCENE,
	END_SCENE
}			SCENE;

typedef struct s_game
{
	t_window	*window;
	t_config	window_config;
	t_game_scene game_scene;
	t_menu_scene menu_scene;
	int 		fps;
	SCENE 		current_scene;
	bool		should_exit;
	bool		aa;
	bool        debug;
} t_game;

void init_game(t_game *game);
void run(t_game *game);
void free_game(t_game *game);

#endif