//
// Created by kohlten on 1/17/19.
//

#ifndef asteroids_UFO_H
#define asteroids_UFO_H

#include "timer.h"
#include "vector/physics_object.h"
#include "surface.h"
#include "data_structures/vector_array.h"

typedef struct          s_ufo {
    t_physics_object object;
    t_surface       *sprite;
    double          angle;
    vector          bullets;
    t_timer         shoot_timer;
    t_timer         move_time;
    int             spin_direction;
    int             health;
    bool            died;
}                       t_ufo;

t_ufo *new_ufo(t_surface *sprite, bool side, t_vector2i window_size, int health);
void update_ufo(t_ufo *ufo, t_vector2i window_size, t_vector2f player_pos, t_surface *bullet_sprite, vector *player_bullets,
                vector *asteroids, bool player_spawned) ;
void draw_ufo(t_ufo *ufo, SDL_Renderer *renderer, bool aa, bool debug);
void free_ufo(t_ufo *ufo);

#endif //asteroids_UFO_H
