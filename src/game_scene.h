//
// Created by kohlten on 1/14/19.
//

#ifndef asteroids_GAME_SCENE_H
#define asteroids_GAME_SCENE_H

#include "fps.h"
#include "window.h"
#include "rendering/text/text.h"
#include "rendering/text/text_collage.h"
#include "asteroid.h"
#include "player.h"

typedef struct s_game_scene {
    t_surface       *player_surf;
    t_surface       *ufo_bullet_surf;
    t_player	    *player;
    t_texture	    *background;
    vector          *asteroid_images;
    vector 		    asteroids;
    vector          ufo_surfs;
    vector          ufos;
    t_fps           update_fps;
    t_fps           render_fps;
    t_text_collage  text;
    char            *update_fps_string;
    char            *render_fps_string;
    char            *player_score_string;
    float 		    background_pos;
    int             round;
    int             created_ufos;
    bool		    paused;
    bool            exited;
    bool            inited;
}               t_game_scene;

void init_game_scene(t_game_scene *scene, t_window *window, int fps);
void reset_game_scene(t_game_scene *scene, t_vector2i window_size);
void game_scene_events(t_game_scene *scene, int type, int key);
void game_scene_update(t_game_scene *scene, t_vector2i window_size);
void game_scene_draw(t_game_scene *scene, SDL_Renderer *renderer, t_vector2i window_size, bool aa, bool debug);
void game_scene_free(t_game_scene *scene);

#endif //asteroids_GAME_SCENE_H
