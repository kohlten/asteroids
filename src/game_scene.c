//
// Created by kohlten on 1/14/19.
//

#include <SDL2_gfxPrimitives.h>

#include "game_scene.h"
#include "assert.h"
#include "libft.h"
#include "image_io.h"
#include "rendering/basic_shapes.h"
#include "ufo.h"


// @TODO Make this smaller or separate it out


static void create_new_ufo(t_game_scene *scene, t_vector2i window_size) {
    t_ufo *tmp;
    int sprite_num;
    bool side;

    sprite_num = (int) map(randint(0, 100), new_vector2f(0, 100), new_vector2f(0, vector_total(&scene->ufo_surfs) - 1));
    side = 0;
    if ((int) scene->player->object.pos.y >= window_size.y / 2)
        side = 1;
    tmp = new_ufo(vector_get(&scene->ufo_surfs, sprite_num), side, window_size, scene->round);
    assert(tmp != NULL, "Failed to make a new ufo!\n");
    vector_add(&scene->ufos, tmp);
    scene->created_ufos++;
}

static void reset_round(t_game_scene *scene, t_vector2i window_size) {
    spawn_new_asteroids(&scene->asteroids, window_size,
                        scene->player->object.pos, scene->asteroid_images);
    scene->round++;
}

void init_game_scene(t_game_scene *scene, t_window *window, int fps) {
    t_surface *tmp;

    scene->player = new_player(new_vector2i(window->width, window->height));
    assert(scene->player != NULL, "Failed to allocate the player!\n");
    scene->asteroid_images = load_images("images/asteroid_images.json");
    assert(scene->asteroid_images != NULL, "Failed to load images!\n");
    init_fps(&scene->update_fps, 60);
    init_fps(&scene->render_fps, fps);
    scene->background = new_texture(window->SDLrenderer, "images/resized_background.png");
    assert(scene->background != NULL, "Failed to load background!\n");
    scene->background_pos = 0;
    vector_init(&scene->asteroids);
    scene->player_surf = get_current_image_animator(&scene->player->animator);
    vector_init(&scene->ufos);
    vector_init(&scene->ufo_surfs);
    tmp = new_surface("images/PNG/ufoBlue.png");
    assert(tmp != NULL, "Failed to load UFOS!\n");
    vector_add(&scene->ufo_surfs, tmp);
    tmp = new_surface("images/PNG/ufoRed.png");
    assert(tmp != NULL, "Failed to load UFOS!\n");
    vector_add(&scene->ufo_surfs, tmp);
    tmp = new_surface("images/PNG/ufoGreen.png");
    assert(tmp != NULL, "Failed to load UFOS!\n");
    vector_add(&scene->ufo_surfs, tmp);
    tmp = new_surface("images/PNG/ufoYellow.png");
    assert(tmp != NULL, "Failed to load UFOS!\n");
    vector_add(&scene->ufo_surfs, tmp);
    scene->ufo_bullet_surf = new_surface("images/PNG/Lasers/laserRed01.png");
    assert(scene->ufo_bullet_surf != NULL, "Failed to load lasers!\n");
    assert(init_text_collage_json(&scene->text, "fonts/fasttracker2-style_12x12.json",
                                  scene->ufo_bullet_surf->format) == 0,
           "Failed to init text collage!\n");
    scene->inited = false;
    scene->round = 1;
    scene->created_ufos = 0;
    scene->player_score_string = NULL;
    scene->update_fps_string = NULL;
    scene->render_fps_string = NULL;
}

void reset_game_scene(t_game_scene *scene, t_vector2i window_size) {
    int i;

    scene->inited = false;
    reset_player(scene->player, window_size);
    scene->player->lives = 3;
    scene->player->score = 0;
    scene->player->moving = false;
    scene->player->rotating = false;
    scene->player->shielded = false;
    scene->player->shield_energy = 100;
    scene->round = 1;
    scene->created_ufos = 0;
    change_state_animator(&scene->player->animator, "default");
    spawn_new_asteroids(&scene->asteroids, window_size, scene->player->object.pos, scene->asteroid_images);
    scene->inited = true;
    for (i = 0; i < vector_total(&scene->ufos); i++) {
        free_ufo(vector_get(&scene->ufos, i));
        vector_delete(&scene->ufos, i);
    }
}

void game_scene_events(t_game_scene *scene, int type, int key) {
    if (scene->inited) {
        if (type == SDL_KEYDOWN) {
            if (key == SDLK_UP)
                change_dir(scene->player, 'w', 1);
            else if (key == SDLK_LEFT)
                change_dir(scene->player, 'a', 1);
            else if (key == SDLK_RIGHT)
                change_dir(scene->player, 'd', 1);
            else if (key == SDLK_SPACE)
                change_dir(scene->player, ' ', 1);
            else if (key == SDLK_p)
                scene->paused = !scene->paused;
            else if (key == SDLK_g)
                change_dir(scene->player, 'g', 1);
            else if (key == SDLK_r) {
                scene->player->lives--;
            } else if (key == SDLK_DOWN)
                change_dir(scene->player, 's', 1);
        }
        if (type == SDL_KEYUP) {
            if (key == SDLK_UP)
                change_dir(scene->player, 'w', 0);
            else if (key == SDLK_LEFT)
                change_dir(scene->player, 'a', 0);
            else if (key == SDLK_RIGHT)
                change_dir(scene->player, 'd', 0);
            else if (key == SDLK_SPACE)
                change_dir(scene->player, ' ', 0);
            else if (key == SDLK_DOWN)
                change_dir(scene->player, 's', 0);
        }
    }
}

void game_scene_update(t_game_scene *scene, t_vector2i window_size) {
    int fps;
    char *num;
    char *joined;
    int i;
    t_ufo *tmp;

    if (scene->player->lives > 0 && scene->inited && !scene->paused) {
        limit_fps(&scene->update_fps);
        update_fps(&scene->update_fps);
        fps = get_fps(&scene->render_fps);
        if (fps != -1) {
            if (scene->render_fps_string)
                free(scene->render_fps_string);
            num = ft_itoa(fps);
            joined = ft_strjoin("R: ", num);
            scene->render_fps_string = joined;
            free(num);
        }
        fps = get_fps(&scene->update_fps);
        if (fps != -1) {
            if (scene->update_fps_string)
                free(scene->update_fps_string);
            num = ft_itoa(fps);
            joined = ft_strjoin("U: ", num);
            scene->update_fps_string = joined;
            free(num);
        }
        num = ft_itoa(scene->player->score);
        joined = ft_strjoin("Score: ", num);
        if (scene->player_score_string)
            free(scene->player_score_string);
        scene->player_score_string = joined;
        free(num);
        if (scene->background_pos <= -scene->background->width)
            scene->background_pos = 0;
        scene->background_pos -= 0.5;
        if (count_large_asteroids(&scene->asteroids) <= 1 && randint(0, 1000) < 4 && scene->created_ufos < scene->round)
            create_new_ufo(scene, window_size);
        i = 0;
        while (i < vector_total(&scene->ufos)) {
            tmp = vector_get(&scene->ufos, i);
            update_ufo(tmp, window_size, scene->player->object.pos, scene->ufo_bullet_surf, &scene->player->bullets,
                       &scene->asteroids, scene->player->spawned);
            if (tmp->died) {
                free_ufo(tmp);
                vector_delete(&scene->ufos, i);
            } else
                i++;
        }
        update_player(scene->player, &scene->asteroids, &scene->ufos, window_size);
        assert(update_asteroids(&scene->asteroids, window_size,
                                scene->player->object.pos, scene->asteroid_images) == 0,
               "Failed to update asteroids!\n");
        if (vector_total(&scene->asteroids) == 0)
            reset_round(scene, window_size);
        if (scene->player->lives <= 0)
            scene->exited = true;
    }
}

void game_scene_draw(t_game_scene *scene, SDL_Renderer *renderer, t_vector2i window_size, bool aa, bool debug) {
    int i;
    int gui_pos;

    if (scene->player->lives > 0 && scene->inited) {
        limit_fps(&scene->render_fps);
        update_fps(&scene->render_fps);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        render_texture(scene->background, renderer,
                       new_vector2i((int) scene->background_pos + scene->background->width, 0),
                       NULL, 0, new_vector2i(0, 0), SDL_FLIP_NONE);
        render_texture(scene->background, renderer, new_vector2i((int) scene->background_pos, 0), NULL, 0,
                       new_vector2i(0, 0), SDL_FLIP_NONE);
        draw_asteroids(&scene->asteroids, renderer, aa, debug);
        draw_player(scene->player, renderer, aa, debug);
        gui_pos = 50;
        for (i = 0; i < scene->player->lives; i++) {
            rotate_and_draw_surface(scene->player_surf, renderer, new_vector2i(gui_pos, 50), 0, 0.4, aa);
            gui_pos += (int) ((double) scene->player_surf->size.x * 0.4) + 10;
        }
        draw_rect(renderer, new_vector2i(40, 100), new_vector2i(150, 10), aa, 0, 0, 255, 255);
        for (i = 40; i < map(scene->player->shield_energy, new_vector2f(0, 100), new_vector2f(0, 190)); i++) {
            if (aa)
                aalineRGBA(renderer, i, 100, i, 110, 0, 0, 255, 255);
            else
                lineRGBA(renderer, i, 100, i, 110, 0, 0, 255, 255);
        }
        for (i = 0; i < vector_total(&scene->ufos); i++) {
            draw_ufo(vector_get(&scene->ufos, i), renderer, aa, debug);
        }
        if (scene->player_score_string)
            draw_text_collage(&scene->text, scene->player_score_string, renderer,
                              new_vector2i((window_size.x / 2) - 50, 65), 0, 1.0, aa,
                              NULL, 0);
        if (scene->update_fps_string)
            draw_text_collage(&scene->text, scene->update_fps_string, renderer, new_vector2i(window_size.x - 80, 65), 0,
                              0.8, aa, NULL,
                              0);
        if (scene->render_fps_string)
            draw_text_collage(&scene->text, scene->render_fps_string, renderer, new_vector2i(window_size.x - 160, 65),
                              0, 0.8, aa,
                              NULL, 0);
        SDL_RenderPresent(renderer);
    }
}

void game_scene_free(t_game_scene *scene) {
    int i;

    free_text_collage(&scene->text);
    if (scene->render_fps_string) free(scene->render_fps_string);
    if (scene->update_fps_string) free(scene->update_fps_string);
    if (scene->player_score_string) free(scene->player_score_string);
    free_images(scene->asteroid_images, 3, true);
    free_player(scene->player);
    free_asteroids(&scene->asteroids);
    free_texture(scene->background);
    vector_free(&scene->asteroids);
    for (i = 0; i < vector_total(&scene->ufos); i++) {
        free_ufo(vector_get(&scene->ufos, i));
        vector_delete(&scene->ufos, i);
    }
    vector_free(&scene->ufos);
    free_surface(scene->ufo_bullet_surf);
    for (i = 0; i < vector_total(&scene->ufo_surfs); i++) {
        free_surface(vector_get(&scene->ufo_surfs, i));
        vector_delete(&scene->ufo_surfs, i);
    }
    vector_free(&scene->ufo_surfs);
}
