//
// Created by Alexander Strole on 12/1/18.
//

#include <SDL2_gfxPrimitives.h>
#include "asteroid.h"
#include "libft.h"
#include "assert.h"
#include "random.h"

t_asteroid *new_asteroid(t_vector2f pos, t_vector2i window_size, double radius, int size, t_vector2f player_pos, vector *asteroid_images) {
    t_asteroid *asteroid;
    int location;

    asteroid = ft_memalloc(sizeof(t_asteroid));
    if (!asteroid)
        return (NULL);
    init_physics_object(&asteroid->object);
    if (pos.x == -1 && pos.y == -1) {
        while (asteroid->object.pos.x == 0 || dist_vector(asteroid->object.pos, player_pos) <= 200) {
            asteroid->object.pos.x = randint(0, window_size.x);
            asteroid->object.pos.y = randint(0, window_size.y);
        }
        asteroid->object.acc = vector2f_zero;
    } else {
        asteroid->object.pos = pos;
        asteroid->object.acc = vector2f_zero;
    }
    asteroid->size = size;
    while (fabs(asteroid->object.vel.x) <= 0.2)
        asteroid->object.vel.x = map(randint(0, 50), new_vector2f(0.0, 50.0), new_vector2f(-1.7, 1.7));
    while (fabs(asteroid->object.vel.y) <= 0.2)
        asteroid->object.vel.y = map(randint(0, 50), new_vector2f(0.0, 50.0), new_vector2f(-1.7, 1.7));
    asteroid->radius = radius;
    asteroid->total_radius = asteroid->radius;
    asteroid->health = 4 - size;
    init_timer(&asteroid->teleport_wait, 1000);
    init_animator(&asteroid->animator);
    switch (size) {
        case 1:
            location = (int)map((double)randint(0, 500), new_vector2f(0, 500), new_vector2f(0, vector_total(&asteroid_images[0])));
            assert(add_state_animator(&asteroid->animator, "default", vector_get(&asteroid_images[0], location), 50, 0, -1) == 0, "Failed to add state!\n");
            break;
        case 2:
            location = (int)map((double)randint(0, 500), new_vector2f(0, 500), new_vector2f(0, vector_total(&asteroid_images[1])));
            assert(add_state_animator(&asteroid->animator, "default", vector_get(&asteroid_images[1], location), 50, 0, -1) == 0, "Failed to add state!\n");
            break;
        case 3:
            location = (int)map((double)randint(0, 500), new_vector2f(0, 500), new_vector2f(0, vector_total(&asteroid_images[2])));
            assert(add_state_animator(&asteroid->animator, "default", vector_get(&asteroid_images[2], location), 50, 0, -1) == 0, "Failed to add state!\n");
            break;
        default:
            break;
    }
    change_state_animator(&asteroid->animator, "default");
    start_playing_animator(&asteroid->animator);
    return (asteroid);
}

void draw_asteroid(t_asteroid *asteroid, SDL_Renderer *renderer, bool aa, bool debug) {
    t_vector2i frame_size;

    frame_size = get_frame_size(&asteroid->animator);
    draw_animator(&asteroid->animator, renderer, new_vector2i((int)(asteroid->object.pos.x - ((frame_size.x / 2) * 0.7)), (int)(asteroid->object.pos.y - ((frame_size.y / 2) * 0.7))), 0, 0.7, aa);
    if (debug) {
        if (aa)
            aacircleRGBA(renderer, asteroid->object.pos.x, asteroid->object.pos.y, asteroid->radius, 255, 0, 0, 255);
        else
            circleRGBA(renderer, asteroid->object.pos.x, asteroid->object.pos.y, asteroid->radius, 255, 0, 0, 255);
    }
}

void update_asteroid(t_asteroid *asteroid, t_vector2i size) {
    if (timer_done(&asteroid->teleport_wait) && asteroid->object.pos.x < 0) {
        start_timer(&asteroid->teleport_wait);
        asteroid->object.pos.x = size.x;
    } else if (timer_done(&asteroid->teleport_wait) && asteroid->object.pos.x > size.x) {
        start_timer(&asteroid->teleport_wait);
        asteroid->object.pos.x = 0;
    } else if (timer_done(&asteroid->teleport_wait) && asteroid->object.pos.y < 0) {
        start_timer(&asteroid->teleport_wait);
        asteroid->object.pos.y = size.y;
    } else if (timer_done(&asteroid->teleport_wait) && asteroid->object.pos.y > size.y) {
        start_timer(&asteroid->teleport_wait);
        asteroid->object.pos.y = 0;
    }
    update_timer(&asteroid->teleport_wait);
    update_object(&asteroid->object);
    update_animator(&asteroid->animator);
}

void free_asteroid(void *data) {
    t_asteroid *asteroid;

    asteroid = (t_asteroid *)data;
    free_animator(&asteroid->animator, false);
    free(asteroid);
}

void spawn_new_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images) {
    int i;
    t_asteroid *asteroid;

    for (i = 0; i < vector_total(asteroids) - 1; i++) {
        free_asteroid(vector_get(asteroids, i));
        vector_delete(asteroids, i);
    }
    vector_free(asteroids);
    vector_init(asteroids);
    for (i = 0; i < 5; i++) {
        asteroid = new_asteroid(new_vector2f(-1, -1), new_vector2i(window_size.x, window_size.y), 50, 1, player_pos, asteroid_images);
        assert(asteroid != NULL, "Failed to allocate new asteroids!\n");
        vector_add(asteroids, asteroid);
    }
}

int break_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images) {
    int i;
    int j;
    t_asteroid *asteroid;
    t_asteroid *n_asteroid;
    vector new_asteroids;

    vector_init(&new_asteroids);
    for (i = 0; i < vector_total(asteroids); i++) {
        asteroid = vector_get(asteroids, i);
        if (asteroid->health <= 0) {
            if (asteroid->size < 3) {
                for (j = 0; j < 2; j++) {
                    n_asteroid = new_asteroid(asteroid->object.pos, window_size, asteroid->radius / 2,
                            asteroid->size + 1, player_pos, asteroid_images);
                    if (!n_asteroid)
                        return (-1);
                    vector_add(&new_asteroids, n_asteroid);
                }
            }
        }
    }
    for (i = 0; i < vector_total(&new_asteroids); i++)
        vector_add(asteroids, vector_get(&new_asteroids, i));
    vector_free(&new_asteroids);
    return 0;
}

void draw_asteroids(vector *asteroids, SDL_Renderer *renderer, bool aa, bool debug) {
    int i;
    t_asteroid *asteroid;

    for (i = 0; i < vector_total(asteroids); i++) {
        asteroid = vector_get(asteroids, i);
        draw_asteroid(asteroid, renderer, aa, debug);
    }
}

int update_asteroids(vector *asteroids, t_vector2i window_size, t_vector2f player_pos, vector *asteroid_images) {
    int i;
    t_asteroid *asteroid;

    if (break_asteroids(asteroids, window_size, player_pos, asteroid_images) != 0)
        return -1;
    i = 0;
    while (i < vector_total(asteroids)) {
        asteroid = vector_get(asteroids, i);
        update_asteroid(asteroid, window_size);
        if (asteroid->health <= 0) {
            free_asteroid(vector_get(asteroids, i));
            vector_delete(asteroids, i);
        } else
            i++;
    }
    return 0;
}

int count_large_asteroids(vector *asteroids) {
    int i;
    int total;
    t_asteroid *asteroid;

    total = 0;
    for (i = 0; i < vector_total(asteroids); i++) {
        asteroid = vector_get(asteroids, i);
        if (asteroid->size == 1)
            total++;
    }
    return total;
}

void free_asteroids(vector *asteroids) {
    for (int i = 0; i < vector_total(asteroids); i++)
        free_asteroid(vector_get(asteroids, i));
}

