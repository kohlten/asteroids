//
// Created by kohlten on 1/10/19.
//

#include "image_io.h"
#include "game_io.h"
#include "surface.h"
#include "libft.h"

#include <stdbool.h>
#include <jansson.h>

vector *load_images(char *filename) {
    unsigned int i, j, k;
    char *text;
    json_t *images, *root, *arr, *arr1, *string;
    json_error_t error;
    vector *v_images, *tmp;
    t_surface *tmp_surf;


    text = read_file(filename);
    if (!text)
        return NULL;
    root = json_loads(text, 0, &error);
    free(text);
    if (!root) {
        printf("Failed to read file %s. Json Error: line %d: %s\n", filename, error.line, error.text);
        return NULL;
    }
    images = json_object_get(root, "images");
    if (!json_is_array(images)) return NULL;
    v_images = ft_memalloc(sizeof(vector) * json_array_size(images));
    if (!v_images) return NULL;
    for (i = 0; i < json_array_size(images); i++) {
        vector_init(&v_images[i]);
        arr = json_array_get(images, i);
        if (!json_is_array(arr)) return NULL;
        for (j = 0; j < json_array_size(arr); j++) {
            tmp = ft_memalloc(sizeof(vector));
            if (!tmp) return NULL;
            vector_init(tmp);
            arr1 = json_array_get(arr, j);
            if (!json_is_array(arr)) return NULL;
            for (k = 0; k < json_array_size(arr1); k++) {
                string = json_array_get(arr1, k);
                if (!json_is_string(string)) return NULL;
                tmp_surf = new_surface((char *)json_string_value(string));
                if (!tmp_surf) {
                    printf("Failed to load image %s!\n", json_string_value(string));
                    return NULL;
                }
                vector_add(tmp, tmp_surf);
            }
            vector_add(&v_images[i], tmp);
        }
    }
    json_decref(root);
    return v_images;
}

void free_images(vector *images, int length, bool free_textures) {
    int i, j, k;
    vector *arr;

    for (i = 0; i < length; i++) {
        for (j = 0; j < vector_total(&images[i]); j++) {
            arr = vector_get(&images[i], j);
            if (free_textures) {
                for (k = 0; k < vector_total(arr); k++) {
                    free_surface(vector_get(arr, k));
                }
            }
            vector_free(arr);
            free(arr);
        }
    }
    free(images);
}

