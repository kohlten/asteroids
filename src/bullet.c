//
// Created by Alexander Strole on 12/1/18.
//

#include "bullet.h"
#include "libft.h"
#include "assert.h"
#include <SDL.h>
#include <SDL2_gfxPrimitives.h>

t_bullet *new_bullet(t_surface *image, t_vector2f pos, double angle) {
    t_bullet *bullet;

    bullet = ft_memalloc(sizeof(t_bullet));
    if (!bullet)
        return (NULL);
    init_physics_object(&bullet->object);
    bullet->object.pos = pos;
    bullet->angle = angle;
    apply_force(&bullet->object, vector2f_from_angle(angle, 6.8));
    bullet->died = 0;
    bullet->image = image;
    init_timer(&bullet->death_timer, 800);
    start_timer(&bullet->death_timer);
    return (bullet);
}

// @ TODO Make the bullet lifetime a variable of a config
void update_bullet(t_bullet *bullet, t_vector2i size) {
    if (timer_done(&bullet->death_timer))
        bullet->died = 1;
    if (bullet->object.pos.x < 0)
        bullet->object.pos.x = size.x;
    if (bullet->object.pos.x > size.x)
        bullet->object.pos.x = 0;
    if (bullet->object.pos.y < 0)
        bullet->object.pos.y = size.y;
    if (bullet->object.pos.y > size.y)
        bullet->object.pos.y = 0;
    update_object(&bullet->object);
    update_timer(&bullet->death_timer);
}

void draw_bullet(t_bullet *bullet, SDL_Renderer *renderer, bool aa, bool debug) {
    t_surface *rotated;

    (void) debug;
    rotated = rotate_surface(bullet->image, map_constrain(bullet->angle, new_vector2f(0, M_PI * 2), new_vector2f(360, 0)) - 90, 0.5, aa);
    assert(rotated != NULL, "Failed to rotate image!\n");
    draw_surface(rotated, renderer, new_vector2i((int)bullet->object.pos.x - (rotated->size.x / 2), (int)bullet->object.pos.y - (rotated->size.y / 2)));
    free_surface(rotated);
}

void free_bullet(void *data) {
    free(data);
}