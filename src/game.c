#include "game.h"

#include "assert.h"
//#include <pthread.h>

// @TODO Put functions in this file into a different file

static void init_depends() {
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
        fprintf(stderr, "Could not initialize SDL_Image! SDL_image Error: %s\n", IMG_GetError());
        assert(0, "");
    }
    if (!TTF_WasInit()) {
        if (TTF_Init() == -1)
            assert(0, "Failed to initialize TTF!\n");
    } else
        fprintf(stderr, "WARNING: Trying to initialize ttf library twice!\n");
}

static void init_scenes(t_game *game) {
#ifdef PROFILE_CODE
    clock_t start_time;

    start_time = clock();
#endif
    init_menu_scene(&game->menu_scene, game->window, game->fps);
    init_game_scene(&game->game_scene, game->window, game->fps);
#ifdef PROFILE_CODE
    printf("INIT OBJECTS: Took %Lf MS to complete!\n", ((clock() - start_time) / ((long double)CLOCKS_PER_SEC)) * 1000);
#endif
}

static void change_game_scene(t_game *game, SCENE scene) {
    game->current_scene = scene;
}

void init_game(t_game *game) {
    init_depends();
    setup_window(&game->window, &game->fps, &game->window_config);
    if (get_value_config_bool(&game->window_config, "TEXTURE_AA", &game->aa) != CONFIG_SUCCESS)
        game->aa = false;
    if (get_value_config_bool(&game->window_config, "DEBUG", &game->debug) != CONFIG_SUCCESS)
        game->debug = false;
    game->should_exit = false;
    init_scenes(game);
    change_game_scene(game, MENU_SCENE);
}

static inline void get_events(t_game *game) {
    SDL_Event e;
    t_vector2i mouse_pos;
    bool mouse_down;

    mouse_down = false;
    while (SDL_PollEvent(&e)) {
        if (e.type == SDL_QUIT)
            game->should_exit = true;
        if (e.type == SDL_KEYDOWN) {
            if (e.key.keysym.sym == SDLK_ESCAPE)
                game->should_exit = true;
        }
        if (e.type == SDL_MOUSEBUTTONDOWN)
            mouse_down = true;
        SDL_GetMouseState(&mouse_pos.x, &mouse_pos.y);
        if (game->current_scene == GAME_SCENE)
            game_scene_events(&game->game_scene, e.type, e.key.keysym.sym);
        else if (game->current_scene == MENU_SCENE)
            menu_scene_events(&game->menu_scene, mouse_pos, mouse_down);
    }
}

static void *update(void *p) {
    t_game *game;

    game = (t_game *) p;
    //while (game->should_exit != true) {
        if (game->current_scene == MENU_SCENE) {
            menu_scene_update(&game->menu_scene);
            if (game->menu_scene.exited) {
                reset_game_scene(&game->game_scene, new_vector2i(game->window->width, game->window->height));
                change_game_scene(game, GAME_SCENE);
                game->menu_scene.exited = false;
            }
        }
        if (game->current_scene == GAME_SCENE) {
            game_scene_update(&game->game_scene, new_vector2i(game->window->width, game->window->height));
            if (game->game_scene.exited) {
                menu_scene_reset(&game->menu_scene);
                change_game_scene(game, MENU_SCENE);
                game->game_scene.exited = false;
            }
        }
    //}
    return NULL;
}

static void *display(t_game *game) {
    //while (game->should_exit != true) {
        //get_events(game);
        if (game->current_scene == GAME_SCENE)
            game_scene_draw(&game->game_scene, game->window->SDLrenderer,
                            new_vector2i(game->window->width, game->window->height), game->aa, game->debug);
        else if (game->current_scene == MENU_SCENE)
            menu_scene_draw(&game->menu_scene, game->window->SDLrenderer,
                            new_vector2i(game->window->width, game->window->height), game->aa, game->debug);
    //}
    return NULL;
}

void run(t_game *game) {
    //pthread_t update_thread_id;
    //pthread_create(&update_thread_id, NULL, update, game);
    while (!game->should_exit) {
		get_events(game);
		update(game);
		display(game);
    }
    //pthread_join(update_thread_id, NULL);
}

// @TODO Update free to free everything.
void free_game(t_game *game) {
    //menu_scene_free(&game->menu_scene);
    //game_scene_free(&game->game_scene);
    free_config(&game->window_config);
    free_window(game->window);
    IMG_Quit();
    TTF_Quit();
}
