//
// Created by Alexander Strole on 12/1/18.
//

#ifndef asteroids_BULLET_H
#define asteroids_BULLET_H

#include "vector/physics_object.h"
#include <SDL.h>
#include "surface.h"
#include "timer.h"
#include <stdbool.h>

typedef struct      s_bullet
{
    t_surface           *image;
    t_physics_object    object;
    t_timer             death_timer;
    //int                 size;
    bool                died;
    double              angle;
    //long                born;
}                   t_bullet;

t_bullet *new_bullet(t_surface *image, t_vector2f pos, double angle);
void draw_bullet(t_bullet *bullet, SDL_Renderer *renderer, bool aa, bool debug);
void update_bullet(t_bullet *bullet, t_vector2i size);
void free_bullet(void *data);

#endif //asteroids_BULLET_H
