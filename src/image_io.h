//
// Created by kohlten on 1/10/19.
//

#ifndef asteroids_IMAGE_IO_H
#define asteroids_IMAGE_IO_H

#include <SDL.h>
#include "data_structures/vector_array.h"
#include <stdbool.h>

vector *load_images(char *filename);
void free_images(vector *images, int length, bool free_textures);

#endif //asteroids_IMAGE_IO_H
