//
// Created by Alexander Strole on 12/1/18.
//

#ifndef asteroids_PLAYER_H
#define asteroids_PLAYER_H

#include "vector/physics_object.h"
#include "vector/vector.h"
#include "data_structures/vector_array.h"
#include "timer.h"
#include "rendering/animator.h"
#include "texture.h"
#include "surface.h"

typedef struct s_player
{
    t_surface           *bullet_texture;
    t_animator          animator;
    t_animator          shield_animator;
    t_physics_object    object;
    vector              bullets;
    double              dist;
    double              angle;
    double              shield_energy;
    int                 lives;
    bool                moving;
    bool                rotating;
    bool                shooting;
    bool                spawned;
    bool                god_mode;
    bool                shielded;
    int                 rotation_direction;
    int                 added_bullets;
    int                 score;
    t_timer             shield_timer;
    t_timer             death_timer;
    t_timer             bullet_timer;
}               t_player;

t_player *new_player(t_vector2i size);
void draw_player(t_player *player, SDL_Renderer *renderer, bool aa, bool debug);
void change_dir(t_player *player, char dir, int keydown);
void update_player(t_player *player, vector *asteroids, vector *ufos, t_vector2i size);
void shoot(t_player *player);
void reset_player(t_player *player, t_vector2i size);
void free_player(t_player *player);

#endif //asteroids_PLAYER_H
