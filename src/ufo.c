//
// Created by kohlten on 1/17/19.
//

#include <SDL2_gfxPrimitives.h>

#include "libft.h"
#include "assert.h"
#include "ufo.h"
#include "bullet.h"
#include "asteroid.h"

static void change_ufo_dir(t_ufo *ufo) {
    ufo->object.vel = vector2f_from_angle(randint(0, M_PI * 2), 1.0);
    start_timer(&ufo->move_time);
}

static void shoot_ufo(t_ufo *ufo, t_vector2f player_pos, t_surface *bullet_sprite) {
    t_bullet *tmp;
    double angle;
    t_vector2f dir;
    double scale;

    dir = sub_vector2f(player_pos, ufo->object.pos);
    scale = 3.0 / mag_vector(dir);
    dir = mul_by_vector2f(dir, scale);
    angle = heading_vector(dir);
    angle += map(randint(0, 500), new_vector2f(0, 500), new_vector2f(-0.3, 0.3));
    if (angle < 0)
        angle = M_PI * 2 + angle;
    if (angle > M_PI * 2)
        angle = angle - M_PI * 2;
    tmp = new_bullet(bullet_sprite, ufo->object.pos, angle);
    assert(tmp != NULL, "Failed to create bullet!\n");
    vector_add(&ufo->bullets, tmp);
    start_timer(&ufo->shoot_timer);
}

static void collisions_ufo(t_ufo *ufo, vector *player_bullets, vector *asteroids, t_vector2i window_size) {
    t_bullet *tmp;
    t_asteroid *asteroid;
    int i, j;

    for (i = 0; i < vector_total(asteroids); i++) {
        asteroid = vector_get(asteroids, i);
        for (j = 0; j < vector_total(&ufo->bullets); j++) {
            tmp = vector_get(&ufo->bullets, i);
            if (!tmp) continue;
            if (dist_vector(tmp->object.pos, asteroid->object.pos) <= asteroid->radius) {
                tmp->died = 1;
                asteroid->health--;
            }
        }
    }
    i = 0;
    while (i < vector_total(&ufo->bullets)) {
        tmp = vector_get(&ufo->bullets, i);
        update_bullet(tmp, window_size);
        if (tmp->died) {
            free_bullet(tmp);
            vector_delete(&ufo->bullets, i);
        } else
            i++;
    }
    for (i = 0; i < vector_total(player_bullets); i++) {
        tmp = vector_get(player_bullets, i);
        if (dist_vector(tmp->object.pos, ufo->object.pos) <= (double) ufo->sprite->size.x / 2) {
            tmp->died = true;
            if (ufo->health <= 0)
                ufo->died = true;
            ufo->health--;
        }
    }
}

static void update_ufo_info(t_ufo *ufo, t_vector2i window_size) {
    if (ufo->angle < 0)
        ufo->angle = (M_PI * 2);
    if (ufo->angle > M_PI * 2)
        ufo->angle = 0;
    if (ufo->object.pos.x < 0)
        ufo->object.pos.x = window_size.x;
    if (ufo->object.pos.x > window_size.x)
        ufo->object.pos.x = 0;
    if (ufo->object.pos.y < 0)
        ufo->object.pos.y = window_size.y;
    if (ufo->object.pos.y > window_size.y)
        ufo->object.pos.y = 0;
    ufo->angle += 0.1;
}

t_ufo *new_ufo(t_surface *sprite, bool side, t_vector2i window_size, int health) {
    t_ufo *ufo;

    ufo = ft_memalloc(sizeof(t_ufo));
    assert(ufo != NULL, "Failed to malloc ufo\n");
    init_physics_object(&ufo->object);
    if (side == 1)
        ufo->object.pos = vector2i_to_vector2f(window_size);
    change_ufo_dir(ufo);
    init_timer(&ufo->shoot_timer, 2500);
    init_timer(&ufo->move_time, 3000);
    start_timer(&ufo->shoot_timer);
    start_timer(&ufo->move_time);
    vector_init(&ufo->bullets);
    ufo->sprite = sprite;
    ufo->angle = 0;
    ufo->health = health;
    ufo->spin_direction = (int) map(randint(0, 500), new_vector2f(0, 500), new_vector2f(0, 10));
    return ufo;
}


// @TODO Add bullet manager
void update_ufo(t_ufo *ufo, t_vector2i window_size, t_vector2f player_pos, t_surface *bullet_sprite, vector *player_bullets,
           vector *asteroids, bool player_spawned) {
    if (timer_done(&ufo->move_time))
        change_ufo_dir(ufo);
    if (timer_done(&ufo->shoot_timer) && player_spawned)
        shoot_ufo(ufo, player_pos, bullet_sprite);
    collisions_ufo(ufo, player_bullets, asteroids, window_size);
    update_ufo_info(ufo, window_size);
    update_object(&ufo->object);
    update_timer(&ufo->shoot_timer);
    update_timer(&ufo->move_time);
}

void draw_ufo(t_ufo *ufo, SDL_Renderer *renderer, bool aa, bool debug) {
    t_surface *rotated;
    t_bullet *tmp;
    double angle;
    int i;

    if (ufo->spin_direction <= 5)
        angle = map(ufo->angle, new_vector2f(0, M_PI * 2), new_vector2f(0, 360));
    else
        angle = map(ufo->angle, new_vector2f(0, M_PI * 2), new_vector2f(360, 0));
    for (i = 0; i < vector_total(&ufo->bullets); i++) {
        tmp = vector_get(&ufo->bullets, i);
        draw_bullet(tmp, renderer, aa, debug);
    }
    rotated = rotate_surface(ufo->sprite, angle, 0.7, aa);
    draw_surface(rotated, renderer, new_vector2i((int) ufo->object.pos.x - (rotated->size.x / 2),
                                                 (int) ufo->object.pos.y - (rotated->size.y / 2)));
    if (debug) {
        if (aa) {
            aacircleRGBA(renderer, (int) ufo->object.pos.x,
                         (int) ufo->object.pos.y,
                         (int) ((double) ufo->sprite->size.x * 0.7) / 2, 255, 0, 0, 255);
        } else {
            circleRGBA(renderer, (int) ufo->object.pos.x,
                       (int) ufo->object.pos.y,
                       (int) ((double) ufo->sprite->size.x * 0.7) / 2, 255, 0, 0, 255);
        }
    }
    free_surface(rotated);
}

void free_ufo(t_ufo *ufo) {
    int i;
    t_bullet *tmp;

    for (i = 0; i < vector_total(&ufo->bullets); i++) {
        tmp = vector_get(&ufo->bullets, i);
        free_bullet(tmp);
    }
    free(ufo);
}