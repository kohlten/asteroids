#!/usr/bin/env bash
mkdir obj
mkdir subprojects
git clone https://github.com/kohlten/libft.git subprojects/libft
git clone https://gitlab.com/kohlten/libgame.git subprojects/libgame
