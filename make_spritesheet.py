from sys import argv
from os import path, listdir
from PIL import Image

filetypes = ["jpg", "png"]

def paste_images(names, wh, size, output_name):
	print(names)
	for name in names:
		if name.split(".")[-1] not in filetypes:
			print(name)
			print("Unknown image type!")
			return
	names.sort()
	per_row = wh[0] / size
	per_column = wh[1] / size
	images = []
	for name in names:
			images.append(Image.open(name))
	output_im = Image.new('RGBA', wh)
	x_offset = 0
	y_offset = 0
	for im in images:
		output_im.paste(im, (x_offset, y_offset))
		x_offset += size
		if x_offset >= wh[0]:
			x_offset = 0
			y_offset += size
	for im in images:
		im.close()
	output_im.save(output_name)


def main():
	if len(argv) != 5:
		print("python make_spritesheet.py DIRECTORY W:H SIZE OUTPUT_NAME")
		return 0
	file = argv[1]
	if len(argv[2].split(":")) != 2:
		print("Not enough arguments to W:H", argv[2])
		return 0
	try:
		wh = [int(num) for num in argv[2].split(":")]
	except:
		print("invalid W:H")
		return 0
	if not path.isdir(file):
		print("Is not a directory")
		return 0
	try:
		size = int(argv[3])
	except:
		print("Invalid size")
		return 0
	if path.isdir(argv[4]):
		print("Invalid output dir")
		return 0
	if wh[0] % float(size) != 0 or wh[1] % float(size) != 0:
		print("Width and height must be a multiple of size")
		return 0
	files = [file + "/" + name for name in listdir(file)]
	paste_images(files, wh, size, argv[4])

if __name__ == '__main__':
	main()