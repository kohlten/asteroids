OUT = build/asteriods

CFILES = $(wildcard src/*.c)

OFILES = $(patsubst src/%.c,build/obj/%.o,$(CFILES))

JANSSON_LIBS = $(shell pkg-config jansson --libs 2>/dev/null)
SDL2IMAGE_LIBS = $(shell pkg-config SDL2_image --libs 2>/dev/null)
SDL2TTF_LIBS = $(shell pkg-config SDL2_ttf --libs 2>/dev/null)
SDL2_LIBS = $(shell pkg-config sdl2 --libs 2>/dev/null)
SODIUM_LIBS = $(shell pkg-config libsodium --libs 2>/dev/null)
GLEW_LIBS = $(shell pkg-config glew --libs 2>/dev/null)
SDL2_GFX_LIBS = $(shell pkg-config SDL2_gfx --libs 2>/dev/null)

JANSSON_FLAGS = $(shell pkg-config jansson --cflags)
SDL2IMAGE_FLAGS = $(shell pkg-config SDL2_image --cflags)
SDL2TTF_FLAGS = $(shell pkg-config SDL2_ttf --cflags)
SDL2_FLAGS = $(shell pkg-config sdl2 --cflags)
SODIUM_FLAGS = $(shell pkg-config libsodium --cflags)
GLEW_FLAGS = $(shell pkg-config glew --cflags)
SDL2_GFX_FLAGS = $(shell pkg-config SDL2_gfx --cflags 2>/dev/null)

LIBFT_FLAGS = -I subprojects/libgame/include
LIBGAME_FLAGS = -I subprojects/libft/include

LIBGAME_LIBS = -Lsubprojects/libgame/build -lgame
LIBFT_LIBS = -Lsubprojects/libft/build -lft

CC = gcc

LIBS = $(SDL2_LIBS) $(SDL2IMAGE_LIBS) $(SDL2TTF_LIBS) $(JANSSON_LIBS) $(LIBGAME_LIBS) $(SODIUM_LIBS) $(LIBFT_LIBS) $(GLEW_LIBS) $(SDL2_GFX_LIBS) -lm

ifeq ($(shell uname), Darwin)
PLATFORM_DEPENDENCIES = -framework CoreFoundation -framework AppKit -framework CoreAudio \
							-framework CoreVideo -framework ForceFeedback -framework IOKit \
							-framework Metal -framework Carbon -framework AudioToolbox \
							-framework OpenGl -liconv -L /usr/X11/lib/ -lfreetype
endif
ifeq ($(shell uname), Linux)
PLATFORM_DEPENDENCIES = -lX11 -lXext -lasound -lpthread -ldl -lm -lsndio -lcairo -lwayland-server -lwayland-client -lwayland-cursor \
						-lwayland-egl -lpulse -lXcursor -lXi -lXrandr -lXxf86vm -lXinerama -lXss -lxkbcommon -lfreetype
endif

CFLAGS =-g -Wall -Wextra -I src $(LIBFT_FLAGS) $(LIBGAME_FLAGS) $(JANSSON_FLAGS) $(SDL2IMAGE_FLAGS) $(SDL2TTF_FLAGS) $(SDL2_FLAGS) $(SODIUM_FLAGS) $(GLEW_FLAGS) $(SDL2_GFX_FLAGS)

all: check build dependencies $(OFILES)
	$(CC) -o $(OUT) $(PLATFORM_DEPENDENCIES) $(OFILES) $(CFLAGS) $(LIBS)
	cp build/asteriods .

check:
	@[ -z "$(JANSSON_LIBS)" ] && echo "libjansson not found on your system!" && exit 1 || exit 0
	@[ -z "$(SDL2IMAGE_LIBS)" ] && echo "libsdl2_image not found on your system!" && exit 1 || exit 0
	@[ -z "$(SDL2TTF_LIBS)" ] && echo "libsdl2_ttf not found on your system!" && exit 1 || exit 0
	@[ -z "$(SDL2_LIBS)" ] && echo "libsdl2 not found on your system!" && exit 1 || exit 0
	@[ -z "$(SODIUM_LIBS)" ] && echo "libsodium not found on your system!" && exit 1 || exit 0
	@[ -z "$(GLEW_LIBS)" ] && echo "libglew not found on your system!" && exit 1 || exit 0
	@[ -z "$(SDL2_GFX_LIBS)" ] && echo "libsdl2-gfx not found on your system!" && exit 1 || exit 0

dependencies: $(LIBFT) $(LIBGAME)

$(LIBFT):
	@cd subprojects/libft && make

$(LIBGAME):
	-@cd subprojects/libgame && make

build/obj/%.o: src/%.c
	@echo "CC $(CFLAGS) -c -o build/obj/$(notdir $@) $<"
	@$(CC) $(CFLAGS) -c -o build/obj/$(notdir $@) $<

build:
	-@mkdir build
	-@mkdir build/obj

clean:
	-@rm -rf obj
	-@rm -rf platformer
	-@cd subprojects/libft && make clean
	-@cd subprojects/libgame && make clean

fclean: clean
	-@rm -rf dependencies
	-@cd subprojects/libgame && make fclean
	-@cd subprojects/libft && make fclean

re: clean all

.PHONY: all fclean clean re test dependencies build check
